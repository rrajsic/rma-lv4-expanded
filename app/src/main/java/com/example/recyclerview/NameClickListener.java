package com.example.recyclerview;

public interface NameClickListener {
    void onBtnClick(int position);
    void onNameClick(int position);
}
