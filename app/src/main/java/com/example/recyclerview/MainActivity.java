package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener{

    private static final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private List<String> dataList;
    private CustomAdapter customAdapter;
    private EditText addName_editText;
    private Button addName_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupData();
        setupRecyclerView();
        addName_editText = findViewById(R.id.addName);
        addName_button = findViewById(R.id.addName_button);

        addName_button.setOnClickListener(v -> addNameToList());

    }

    private void setupData(){
        dataList = new ArrayList<>();
        dataList.add("Ivan");
        dataList.add("Marija");
    }

    private void setupRecyclerView(){
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        customAdapter = new CustomAdapter(dataList, this);
        recyclerView.setAdapter(customAdapter);
    }

    private void addNameToList(){
        Collections.reverse(dataList);
        dataList.add(addName_editText.getText().toString());
        Collections.reverse(dataList);

        customAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBtnClick(int position) {
        dataList.remove(position);

        customAdapter.notifyDataSetChanged();
    }

    public void onNameClick(int position){
        Intent intent = new Intent(this, NameDetailActivity.class);
        intent.putExtra(NameDetailFragment.ARG_ITEM_ID, dataList.get(position));

        startActivity(intent);
    }
}